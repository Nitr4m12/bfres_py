#!/usr/bin/env python
from extendedio import *

# Structures that appear in 2 or more subfiles

FLAG_MASKS = {
    # Masks for the many flags that can be found inside the file
    "Skeleton": 0b00000000_00000000_00010011_00000000,
    "Bone": 0b11111111_10000111_00010000_00000001
}

GX2_ATTRIBUTE_FORMATS = {
    # Formats available in the GX2AttribFormat enum
    0x0000: "unorm_8",
    0x0004: "unorm_8_8",
    0x0007: "unorm_16_16",
    0x000A: "unorm_8_8_8_8",
    0x0100: "uint_8",
    0x0104: "uint_8_8",
    0x010A: "uint_8_8_8_8",
    0x0200: "snorm_8",
    0x0204: "snorm_8_8",
    0x0207: "snorm_16_16",
    0x020A: "snorm_8_8_8_8",
    0x020B: "snorm_10_10_10_2",
    0x0300: "sint_8",
    0x0304: "sint_8_8",
    0x030A: "sint_8_8_8_8",
    0x0806: "float_32",
    0x0808: "float_16_16",
    0x080D: "float_32_32",
    0x080F: "float_16_16_16_16",
    0x0811: "float_32_32_32",
    0x0813: "float_32_32_32_32",
}

class IndexGroup:
    def __init__(self, stream: Reader):
        self.size: int = stream.readf('I')
        self.count: int = stream.readf('I')

        self.entries = []

        for i in range(self.count + 1):
            self.entries.append(self.Entry(stream))

    class Entry:
        def __init__(self, stream):
            self.search_value: int = stream.readf('i')
            self.left_index: int = stream.readf('H')
            self.right_index: int = stream.readf('H')
            self.name_offset: int = stream.tell() + stream.readf('i')
            self.data_offset: int = stream.tell() + stream.readf('i')

class ResString:
    def __init__(self, reader: Reader):
        self.value = reader.read_string()

class Matrix:
    def __init__(self, count: int, reader: Reader):
        self.values = [reader.readf('H')] * count

class TextureReference:
    def __init__(self, reader: Reader):
        self.texture_name = reader.read_name()
        self.texture_offset = reader.readf('i')

class Buffer:
    def __init__(self, stream: Reader):
        self.data_ptr = stream.readf('I')
        self.size = stream.readf('I')

        self.handle = stream.readf('i')
        self.stride = stream.readf('i')

        self.buffering_count = stream.readf('H')

        self.context_pointer = stream.readf('I')
        
        self.data_offset = stream.tell() + stream.readf('i')

class Vector:
    def __init__(self, reader: Reader, has_w: bool = False):
        self.x: float = reader.readf('f')
        self.y: float = reader.readf('f')
        self.z: float = reader.readf('f')

        if has_w:
            self.w: float = reader.readf('f')

class Transform(Vector):
    ...

class Color:
    def __init__(self, reader: Reader, has_alpha: bool = False):
        self.r: float = reader.readf('f')
        self.g: float = reader.readf('f')
        self.b: float = reader.readf('f')
        if has_alpha:
            self.a: float = reader.readf('f')

class Curve:
    def __init__(self, stream: Reader):
        ...
        
    class Header():
        ...

    class Frames():
        ...

    class Keys():
        ...

    class StepCurves():
        ...

    class LinearCurves():
        ...

class UserData:
    def __init__(self, reader: Reader):
        self.name = reader.read_name()
        count = reader.readf('H')
        self.type = reader.readf('B')
        reader.skip(1)

        data = []

        for _ in range(count):
            if self.type == 0x0:
                data.append(reader.readf('i'))
                
            elif self.type == 0x1:
                data.append(reader.readf('f'))

            elif self.type == 0x2:
                with TemporarySeek(reader, reader.readf('i')):
                    data.append(reader.read_string(encoding="ascii"))

            elif self.type == 0x3:
                with TemporarySeek(reader, reader.readf('i')):
                    data.append(reader.read_string(encoding="utf-16"))

            elif self.type == 0x4:
                data.append(reader.readf('B'))

class ResReader(Reader):
    def read_index(self, Structure):
        # Perform a temporary seek to an index group
        # so that we can read the corresponding file
        curr = self.tell()
        idx_offset = self.readf('i')

        if idx_offset == 0:
            return

        else:
            idx_offset += curr

        with TemporarySeek(self, idx_offset):
            idx_group = IndexGroup(self);
            sub_files = [];

            for entry in idx_group.entries:
                if entry.search_value != -1:
                    self.seek(entry.data_offset);
                    sub_file = Structure(self);
                    sub_files.append(sub_file);
                    
        return sub_files;

    def read_name(self):
        curr = self.tell()
        offset = self.readf('i')

        if offset == 0:
            return;

        else:
            offset += curr;

        with TemporarySeek(self, offset):
            string = self.read_string();
        
        return string;

    def read_subfile(self, SubFile):
        curr = self.tell();
        offset = self.readf('i');

        if offset == 0:
            return;

        else:
            offset += curr;

        with TemporarySeek(self, offset):
            subfile = SubFile(self);

        return subfile;

    def read_data(self, size):
        curr = self.tell()
        offset = self.readf('i')

        if offset == 0:
            return;

        else:
            offset += curr;

        with TemporarySeek(self, offset):
            data = self.read(size);

        return data;

    def read_subfiles(self, SubFile, count: int):
        curr = self.tell();
        offset = self.readf('i');

        if offset == 0:
            return;

        else:
            offset += curr;

        subfiles = []

        with TemporarySeek(self, offset):
            for _ in range(count):
                subfiles.append(SubFile(self));

        return subfiles;