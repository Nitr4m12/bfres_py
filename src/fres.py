#!/usr/bin/env python
from typing import List
from pathlib import Path
import sys

from common import *
import oead

class Dummy:
    ...

class FRES:
    def __init__(self, data: bytes):
        reader: ResReader = ResReader(data, "big");
        self.read(reader);

    def read(self, reader: ResReader):
        magic = reader.read_string(4)
        assert magic == "FRES"

        self.version = reader.readf('I')
        self.bom = reader.readf('H')
        self.size = reader.readf('H')
        self.file_size = reader.readf('I')
        self.alignment = reader.readf('I')
        self.name = reader.read_name()
        self.str_table_size = reader.readf('i')
        self.str_table_offset = reader.tell() + reader.readf('i')

        # Read FMDL
        self.models = reader.read_index(self.Model)

        # Read FTEX
        self.textures = reader.read_index(self.Texture)

        # Read FSKA
        self.skeleton_animations = reader.read_index(self.SkeletalAnimation)

        # Read FSHU
        self.shader_param_anims = reader.read_index(self.ShaderParamAnim)

        # Read FSHU
        self.color_anims = reader.read_index(self.ColorAnim)

        # Read FSHU
        self.texture_srts = reader.read_index(self.TextureSRTAnim)

        # Read FTXP
        self.texture_patterns = reader.read_index(self.TexturePatternAnim)

        # Read FVIS
        self.bone_vis = reader.read_index(self.BoneVisAnim)

        # Read FVIS
        self.material_vis = reader.read_index(self.MaterialVisAnim)

        # Read FSHA
        self.shape_anims = reader.read_index(self.ShapeAnim)

        # Read FSCN
        self.scene_anims = reader.read_index(self.SceneAnim)

        # Read EmbeddedFiles
        self.embedded_files = reader.read_index(self.EmbeddedFile)

        self.dict_counts = []
        for j in  range(12):
            self.dict_counts.append(reader.readf('H'))

        self.usr_ptr = reader.readf('I')

    class Model:
        # FMDL: caFe MoDeL
        def __init__(self, reader: ResReader):
            magic = reader.read_string(4)
            assert magic == "FMDL"
            
            self.file_name = reader.read_name()
            self.file_path = reader.readf('i') # Always 0

            self.skeleton = reader.read_subfile(self.Skeleton)

            vertices_offset = reader.tell() + reader.readf('i')
            
            # Parse subfiles
            self.shapes = reader.read_index(self.Shape)
            self.materials = reader.read_index(self.Material)
            self.user_data: UserData = reader.read_index(UserData)

            vertex_count = reader.readf('H')
            shape_count = reader.readf('H')
            material_count = reader.readf('H')
            user_data_count = reader.readf('H')
            vtx_to_process = reader.readf('I')
            user_pointer = reader.readf('I')

            # Read the vertex array
            self.vertices = []
            with TemporarySeek(reader, vertices_offset):
                for i in range(vertex_count):
                    self.vertices.append(self.Vertex(reader))

        class Vertex:
            def __init__(self, reader: ResReader):
                # FVTX = caFe VerTeX
                magic = reader.read_string(4)
                assert magic == "FVTX"

                attrib_count = reader.readf('B')
                buffer_count = reader.readf('B')
                self.section_index = reader.readf('H')
                vertex_count = reader.readf('I')
                vertex_skin_count = reader.readf('B')

                reader.skip(3)

                self.attributes = reader.read_subfiles(self.Attribute, attrib_count)
                attribs_index = reader.readf('i')

                self.buffers = reader.read_subfiles(self.VertexBuffer, buffer_count)

                user_pointer = reader.readf('I')

            class Attribute:
                def __init__(self, reader: ResReader):
                    self.name = reader.read_name()
                    self.buffer_index = reader.readf('B')

                    reader.skip(1)

                    self.buffer_offset = reader.readf('h')
                    self.format = GX2_ATTRIBUTE_FORMATS[reader.readf('I')]

            class VertexBuffer(Buffer):
                ...

        class Skeleton:
            # FSKL: caFe SKeLeton
            def __init__(self, reader: ResReader):
                scale_types = {
                    '0': "None",
                    '1': "Standard",
                    '2': "Maya",
                    '3': "Softimage"
                }

                magic: bytes = reader.read_string(4)
                assert magic == "FSKL"

                # Unpack flags: xxxxxxxx xxxxxxxx xxxRxxSS xxxxxxxx
                flags: int = reader.readf('I')
                scale_flag: int = flags >> 8 & 0b11
                self.scaling = scale_types[str(scale_flag)]
                self.rotation: str = "Euler_XYZ" if flags >> 12 & 0b1 == 1 else "Quaternion"

                self.bone_count: int = reader.readf('H')
                self.smooth_matrix_count: int = reader.readf('H')
                self.rigid_matrix_count: int = reader.readf('H')

                reader.skip(2)

                self.bones: int = reader.read_index(self.Bone)
                self.bones_offset: int = reader.tell() + reader.readf('i')
                # What the heck is a smooth matrix
                self.smooth_index: int = reader.tell() + reader.readf('i')
                self.smooth_matrix: int = reader.tell() + reader.readf('i')
                self.usr_pointer: int = reader.readf('I')

            class Bone:
                def __init__(self, reader: ResReader):
                    billboarding_ctrl: dict = {
                        '0': "No Billboarding",
                        '1': "Chill Billboarding",
                        '2': "World View Vector",
                        '3': "World View Point",
                        '4': "Screen View Vector",
                        '5': "Screen View Point",
                        '6': "Y-Axis View Vector",
                        '7': "Y-Axis View Point",
                    }

                    self.name: str = reader.read_name()
                    bone_index: int = reader.readf('H')
                    parent_index: int = reader.readf('H')
                    smooth_matrix_index: int = reader.readf('h')
                    rigid_matrix_index: int = reader.readf('h')
                    billboard_index: int = reader.readf('h')
                    user_data_count: int = reader.readf('H')

                    # CCCCTTTT TxxxxBBB xxxRxxxx xxxxxxxV
                    # V = Visibility of the Influenced shape
                    # R = Rotation Mode. Euler XYZ if set, Quaternion if unset
                    # BBB = Projection of billboarded bones
                    # T TTTT = Transform
                    # CCCC = Transform for a hierarchy of bones

                    flags: int = reader.readf('I')
                    self.visibility: bool = bool(flags & 0b1)
                    self.rotation: str = "Euler_XYZ" if flags >> 12 & 0b1 == 1 else "Quaternion"
                    self.billboarding: str = billboarding_ctrl[str(flags >> 16 & 0b111)]
                    self.bone_transform: self.BoneTransform = self.BoneTransform(flags)
                    self.bone_hierarchy_transform: self.BoneTransform(flags)

                    self.scale_vector: Vector = Vector(reader)
                    self.rotation_vector: Vector = Vector(reader, True)
                    self.translation_vector = Vector(reader)
                    self.user_data: UserData = reader.read_index(UserData)

                class BoneTransform:
                    def __init__(self, flags: int):
                        self.segment_scale_compensation: bool = bool(flags >> 23 & 0b1)
                        self.uniform_scale: bool = bool(flags >> 24 & 0b1)
                        self.scale_volume_by_one: bool = bool(flags >> 25 & 0b1)
                        self.no_rotation: bool = bool(flags >> 26 & 0b1)
                        self.no_translation: bool = bool(flags >> 27 & 0b1)
                
                class BoneHierarchyTransform:
                    def __init__(self, flags: int):
                        self.uniform_scale: bool = bool(flags >> 28 & 0b1)
                        self.scale_volume_by_one: bool = bool(flags >> 29 & 0b1)
                        self.no_rotation: bool = bool(flags >> 30 & 0b1)
                        self.no_translation: bool = bool(flags >> 31 & 0b1)

            class SmoothMatrix(Matrix):
                ...

            class RigidMatrix(Matrix):
                ...

        class Shape:
            #FSHP: caFe SHaPe
            def __init__(self, reader: ResReader):
                magic: str = reader.read_string(4)
                assert magic == "FSHP"

                self.poly_name: str = reader.read_name()

                flags: int = reader.readf('I')
                self.has_vertex_buffer: bool = bool(flags & 0b1)
                self.consistent_sub_mesh_boundary: bool = bool(flags >> 1 & 0b1)

                self.section_index: int = reader.readf('H')
                self.mat_index: int = reader.readf('H')
                self.skl_index: int = reader.readf('H')
                self.vtx_index: int = reader.readf('H')
                skl_index_count: int = reader.readf('H')

                self.vtx_skin_count: int = reader.readf('B')
                lod_model_count: int = reader.readf('B')
                key_shape_count: int = reader.readf('B')
                self.target_attrib_count: int = reader.readf('B')
                sub_mesh_bounding_node_count: int = reader.readf('H')

                self.bounding_box_radius: float = reader.readf('f')

                self.fvtx_offset: int = reader.tell() + reader.readf('i')
                self.lod_models: self.LoDModel = reader.read_subfiles(self.LoDModel, lod_model_count)
                self.fskl_index_array: List[self.FSKLIndex] = reader.read_subfiles(self.FSKLIndex, skl_index_count)
                self.key_shapes: int = reader.read_index(self.KeyShape)

                if sub_mesh_bounding_node_count == 0:
                    sub_mesh_bounding_node_count = len(self.lod_models) + sum([x.sub_mesh_count for x in self.lod_models])
                    self.sub_mesh_boundings = reader.read_subfiles(self.Bounding, sub_mesh_bounding_node_count)

                else:
                    self.sub_mesh_bounding_nodes = reader.read_subfiles(self.BoundingNode, sub_mesh_bounding_node_count)
                    self.sub_mesh_boundings = reader.read_subfiles(self.Bounding, sub_mesh_bounding_node_count)
                    self.sub_mesh_bounding_indices = reader.read_subfiles(self.BoundingIndex, sub_mesh_bounding_node_count)

                self.user_pointer: int = reader.readf('I')
            
            class LoDModel:
                def __init__(self, reader: ResReader):
                    self.primitive_type: int = reader.readf('I')
                    self.index_format: int = reader.readf('I')
                    self.point_count: int = reader.readf('I')
                    self.sub_mesh_count: int = reader.readf('H')
                    reader.skip(2)
                    self.sub_meshes: List[self.SubMesh] = reader.read_subfiles(self.SubMesh, self.sub_mesh_count)
                    self.index_buffer: self.IndexBuffer = reader.read_subfile(self.IndexBuffer)
                    self.vtx_skip_count: int = reader.readf('I')

                class SubMesh:
                    def __init__(self, reader: ResReader):
                        self.offset = reader.readf('I')
                        self.count = reader.readf('I')

                class IndexBuffer(Buffer):
                    ...

            class FSKLIndex:
                def __init__(self, reader: ResReader):
                    self.index = reader.readf('H')

            class KeyShape:
                def __init__(self, reader: ResReader):
                    self.target_attrib_indices = reader.readf('20B')
                    self.target_attrib_index_offsets = reader.readf('4B')

            class Bounding:
                def __init__(self, reader: ResReader):
                    self.center: float = Vector(reader)
                    self.extent: float = Vector(reader)

            class BoundingNode:
                def __init__(self, reader: ResReader):
                    self.left_child_index = reader.readf('H')
                    self.right_child_index = reader.readf('H')
                    self.unknown = reader.readf('H')
                    self.next_sibling = reader.readf('H')
                    self.sub_mesh_index = reader.readf('H')
                    self.sub_mesh_count = reader.readf('H')
            
            class BoundingIndex:
                def __init__(self, reader: ResReader):
                    self.index = reader.readf('H')
        
        class Material:
            #FMAT: caFe MATerial
            def __init__(self, reader: ResReader):
                magic: str = reader.read_string(4)
                assert magic == "FMAT"

                self.name: str = reader.read_name()
                self.visible: bool = bool(reader.readf('I') & 0b1)
                self.section_index: int = reader.readf('H')

                render_info_count: int = reader.readf('H')
                texture_ref_count: int = reader.readf('B')
                texture_sampler_count: int = reader.readf('B')
                material_param_count: int = reader.readf('H')
                self.volatile_param_count: int = reader.readf('H')

                self.material_param_size: int = reader.readf('H')
                self.raw_param_size: int = reader.readf('H')
                self.usr_data_entry_count: int = reader.readf('H')

                self.render_info: self.RenderInfo = reader.read_index(self.RenderInfo)
                self.render_state: self.RenderState = reader.read_subfile(self.RenderState)
                self.shader_assign: self.ShaderAssign = reader.read_subfile(self.ShaderAssign)

                self.texture_refs: List[TextureReference] = reader.read_subfiles(TextureReference, texture_ref_count)
                self.texture_samplers: List[self.TextureSampler] = reader.read_subfiles(self.TextureSampler, texture_sampler_count);

                self.textures_sampler_dict_offset: int = reader.tell() + reader.readf('i')

                self.material_params: List[self.MaterialParameter] = reader.read_subfiles(self.MaterialParameter, material_param_count)
                self.material_params_dict_offset: int = reader.tell() + reader.readf('i')
                self.material_params_data_offset: int = reader.tell() + reader.readf('i')

                self.user_data: UserData = reader.read_index(UserData)
                self.volatile_flags_offset: int = reader.tell() + reader.readf('i')
                self.user_pointer: int = reader.readf('I')

            class RenderInfo:
                def __init__(self, reader: ResReader):
                    self.array_length: int = reader.readf('H')
                    self.element_type: int = reader.readf('B')
                    reader.skip(1)
                    self.variable_name: str = reader.read_name()
                    self._get_array_data(reader)

                def _get_array_data(self, reader: ResReader):
                    self.array_data = []
                    for _ in range(self.array_length):
                        if self.element_type == 0:
                            self.array_data.append(reader.readf("2I"))

                        if self.element_type == 1:
                            self.array_data.append(reader.readf("2f"))

                        if self.element_type == 2:
                            self.array_data.append(reader.read_name())

            class TextureSampler:
                def __init__(self, reader: ResReader):
                    self.unpack_gx2(reader)
                    self.handle: int = reader.readf('I') # Set at runtime

                    self.attribute_name: str = reader.read_name()
                    print(self.attribute_name)
                    self.element_index: int = reader.readf('B')
                    reader.skip(3)

                def unpack_gx2(self, reader: ResReader):
                    # xxxJJJxx IIHHHGGF FxEExDDC CCBBBAAA
                    value1: int = reader.readf('I')

                    self.clamp_x: int = value1 & 0b111
                    self.clamp_y: int = value1 >> 3 & 0b111
                    self.clamp_z: int = value1 >> 6 & 0b111
                    self.expand_xy: int = value1 >> 9 & 0b11
                    self.shrink_xy: int = value1 >> 12 & 0b11
                    self.z_filter: int = value1 >> 15 & 0b11
                    self.mip_filter: int = value1 >> 17 & 0b11
                    self.anisotopic_ratio: int = value1 >> 19 & 0b111
                    self.border_type: int = value1 >> 22 & 0b11
                    self.compare_func: int = value1 >> 26 & 0b111

                    # CCCCCCCC CCCCBBBB BBBBBBAA AAAAAAAA
                    value2: int = reader.readf('I')

                    self.min_lod: float = (value2 & 0b11_11111111) / 64
                    self.max_lod: float = (value2 >> 10 & 0b11_11111111) / 64
                    self.lod_bias: float = (value2 >> 20 & 0b1111_11111111) / 64

                    # xAxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
                    value3: int = reader.readf('I')

                    self.enable_depth_cmp: bool = bool(value3 >> 30 & 0b1)

            class MaterialParameter:
                def __init__(self, reader: ResReader):
                    self.type: int = reader.readf('B')
                    self.size: int = reader.readf('B')
                    self.data_offset: int = reader.tell() + reader.readf('H')
                    self.uniform_var_offset: int = reader.readf('i')
                    self.conversion_callback_ptr: int = reader.readf('I')
                    self.param_index: int = reader.readf('H')
                    self.param_index_copy: int = reader.readf('H')
                    self.variable_name: str = reader.read_name()

            class RenderState:
                def __init__(self, reader: ResReader):
                    # xxxxxxxx xxxxxxxx xxxxxxxx xxBBxxMM
                    ## MM = Mask Mode
                    ## BB = Blend Mode
                    self.flags: int = reader.readf('I')

                    self._unpack_gx2_ctrl(reader)

                    self.blend_color: Color = Color(reader, True)

                def _unpack_flags(self, reader: ResReader):
                    mode = self.flags & 0b11
                    blend = self.flags >> 4 & 0b11

                    # Mode
                    if mode == 0b01:
                        self.mode = "Opaque"

                    elif mode == 0b10:
                        self.mode = "Alpha Mask"

                    elif mode == 0b11:
                        self.mode = "Translucent"

                    else:
                        self.mode = "Custom"

                    # Blend
                    if blend == 0b01:
                        self.blend = "Color"

                    elif blend == 0b10:
                        self.blend = "Logical"
                    
                    else:
                        self.blend = "None"


                def _unpack_gx2_ctrl(self, reader: ResReader):
                    # TODO: Give proper names to the placeholders

                    # Polygon Control
                    # xxxxxxxx xxxxxxxx xxIHGFFF EEEDDCBA
                    poly_ctrl = reader.readf('I')
                    self.placeholder1 = poly_ctrl & 0b1
                    self.placeholder2 = poly_ctrl >> 1 & 0b1
                    self.placeholder3 = poly_ctrl >> 2 & 0b1
                    self.placeholder4 = poly_ctrl >> 3 & 0b11
                    self.placeholder5 = poly_ctrl >> 5 & 0b111
                    self.placeholder6 = poly_ctrl >> 8 & 0b111
                    self.placeholder7 = poly_ctrl >> 11 & 0b1
                    self.placeholder8 = poly_ctrl >> 12 & 0b1
                    self.placeholder9 = poly_ctrl >> 13 & 0b1

                    # Depth Control
                    # MMMLLLKK KJJJIIIH HHGGGFFF EDDDxCBA
                    depth_ctrl = reader.readf('I')
                    self.placeholder10 = depth_ctrl & 1
                    self.placeholder11 = depth_ctrl >> 1 & 0b1
                    self.placeholder12 = depth_ctrl >> 2 & 0b1
                    self.placeholder13 = depth_ctrl >> 4 & 0b111
                    self.placeholder14 = depth_ctrl >> 7 & 0b1
                    self.placeholder15 = depth_ctrl >> 8 & 0b111
                    self.placeholder16 = depth_ctrl >> 11 & 0b111
                    self.placeholder17 = depth_ctrl >> 14 & 0b111
                    self.placeholder18 = depth_ctrl >> 17 & 0b111
                    self.placeholder19 = depth_ctrl >> 20 & 0b111
                    self.placeholder20 = depth_ctrl >> 23 & 0b111
                    self.placeholder21 = depth_ctrl >> 26 & 0b111
                    self.placeholder22 = depth_ctrl >> 29 & 0b111

                    # Alpha Test
                    # xxxxxxxx xxxxxxxx xxxxxxxx xxxxBAAA
                    alpha_test = reader.readf('I')
                    self.placeholder23 = alpha_test & 0b111
                    self.placeholder24 = alpha_test >> 3 & 0b1

                    # Color Control
                    # xxxxxxxx DDDDDDDD CCCCCCCC xBBBxxxA
                    color_ctrl = reader.readf('I')
                    self.placeholder25 = color_ctrl & 0b1
                    self.placeholder26 = color_ctrl >> 4 & 0b111
                    self.placeholder27 = color_ctrl >> 8 & 0b11111111
                    self.placeholder28 = color_ctrl >> 16 & 0b11111111

                    # Blend Control
                    # xxGFFFFF EEEDDDDD xxxCCCCC BBBAAAAA
                    blend_ctrl = reader.readf('I')
                    self.placeholder29 = blend_ctrl & 0b11111
                    self.placeholder30 = blend_ctrl >> 5 & 0b111
                    self.placeholder31 = blend_ctrl >> 8 & 0b11111
                    self.placeholder32 = blend_ctrl >> 16 & 0b11111
                    self.placeholder33 = blend_ctrl >> 21 & 0b111
                    self.placeholder34 = blend_ctrl >> 24 & 0b11111
                    self.placeholder35 = blend_ctrl >> 29 & 0b1

            class ShaderAssign:
                def __init__(self, reader: ResReader):
                    self.shader_archive_name: str = reader.read_name()
                    self.shading_model_name: int = reader.read_name()
                    
                    self.revision: int = reader.readf('I')

                    attribute_assign_count: int = reader.readf('B')
                    sampler_assign_count: int = reader.readf('B')
                    shader_option_count: int = reader.readf('H')

                    self.attribute_assigns: List[ResString] = reader.read_index(ResString)
                    self.sampler_assigns: List[ResString] = reader.read_index(ResString)
                    self.shader_options: List[ResString] = reader.read_index(ResString)


    class Texture:
        # FTEX: caFe TEXture
        def __init__(self, reader: ResReader):
            magic: str = reader.read_string(4)
            assert magic == "FTEX"

            self.dimension: int = reader.readf('I')
            self.width: int = reader.readf('I')
            self.height: int = reader.readf('I')
            self.depth: int = reader.readf('I')

            self.mipmap_count: int = reader.readf('I')
            self.format: int = reader.readf('I')
            self.aa_mode: int = reader.readf('I')
            self.usage: int = reader.readf('I')

            data_length: int = reader.readf('I')
            self.data_pointer: int = reader.readf('I')
            mipmaps_data_length: int = reader.readf('I')
            self.mipmaps_pointer: int = reader.readf('I')

            self.tile_mode: int = reader.readf('I')
            self.swizzle_value: int = reader.readf('I')
            self.alignment: int = reader.readf('I')
            self.pitch: int = reader.readf('I')

            self.mipmaps_offsets: List[int] = []
            for i in range(13):
                self.mipmaps_offsets.append(reader.readf('I'))

            self.first_mipmap: int = reader.readf('I')
            self.mipmap_count_copy: int = reader.readf('I')

            self.first_slice: int = reader.readf('I')
            self.slice_count: int = reader.readf('I')

            self.component_selector: self.ComponentSelector = self.ComponentSelector(reader)
            
            self.texture_registers: List[int] = []
            for i in range(5):
                self.texture_registers.append(reader.readf('I'))
            
            self.texture_handle: int = reader.readf('I')
            self.array_length: int = reader.readf('B')

            reader.skip(3)

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.readf('i')
            self.data: bytes = reader.read_data(data_length)
            self.mipmap_data: bytes = reader.read_data(mipmaps_data_length)

            self.user_data: List[UserData] = reader.read_index(UserData)
            user_data_entry_count: int = reader.readf('H')
            reader.skip(2)

        class ComponentSelector:
            def __init__(self, reader: ResReader):
                self.r = reader.readf('B')
                self.g = reader.readf('B')
                self.b = reader.readf('B')
                self.a = reader.readf('B')

    class SkeletalAnimation:
        # FSKA: caFe SKeletal Animation
        def __init__(self, reader: ResReader):
            magic: str = reader.read_string(4)
            assert magic == "FSKA"

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.readf('i')
            self.flags: int = reader.readf('I')
            self.frame_count: int = reader.readf('I')
            bone_anim_count: int = reader.readf('H')
            self.usr_data_entry_count: int = reader.readf('H')
            self.curve_count: int = reader.readf('I')
            self.baked_length: int = reader.readf('I')
            self.bone_anims: List[BoneAnimation] = reader.read_subfiles(self.BoneAnimation, bone_anim_count);
            self.skeleton_offset: int = reader.tell() + reader.readf('i')
            self.bind_index_array_offset: int = reader.tell() + reader.readf('i') #TODO: Set the data for the bind index array
            self.usr_data: UserData = reader.read_index(UserData);
    
        class BoneAnimation:
            def __init__(self, reader: ResReader):
                self.flags: int = reader.readf('I')
                self.bone_name: int = reader.read_name()
                self.start_rotation: int = reader.readf('B')
                self.start_translation: int = reader.readf('B')
                curve_count: int = reader.readf('B')
                self.base_data_translate_offset: int = reader.readf('B')
                self.start_curve_index: int = reader.readf('B')
                reader.skip(3)
                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.base_data_offset = reader.tell() + reader.readf('i')

    class ShaderParamAnim:
        # FSHU: caFe SHader parameter animation Uber
        def __init__(self, reader: ResReader):
            magic: str = reader.read_string(4)
            assert magic == 'FSHU'

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.tell() + reader.readf('i')

            self.flags: int = reader.readf('I')

            self.frame_count: int = reader.readf('I')
            mat_anim_count: int = reader.readf('H')
            self.user_data_entry_count: int = reader.readf('h')
            self.param_anim_info_count: int = reader.readf('I')
            curve_count: int = reader.readf('I')

            self.baked_size: int = reader.readf('I')

            self.model_offset: int = reader.tell() + reader.readf('i')
            self.bind_index_offset: int = reader.tell() + reader.readf('i')

            self.material_anims: List[self.MaterialAnimation] = reader.read_subfiles(self.MaterialAnimation, mat_anim_count)

            self.user_data: List[UserData] = reader.read_index(UserData)

        class MaterialAnimation:
            def __init__(self, reader: ResReader):
                param_anim_info_count: int = reader.readf('H')
                curve_count: int = reader.readf('H')
                anim_constant_count: int = reader.readf('H')

                reader.skip(2)

                self.start_curve_index: int = reader.readf('i')
                self.start_param_anim_info_index: int = reader.readf('i')
                self.anim_name: str = reader.read_name()

                self.parameter_anim_infos: List[self.ParameterAnimationInfo] = reader.read_subfiles(self.ParameterAnimationInfo, param_anim_info_count)
                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.animation_constants: List[self.AnimationConstant] = reader.read_subfiles(self.AnimationConstant, anim_constant_count)

            class ParameterAnimationInfo:
                def __init__(self, reader: ResReader):
                    self.start_curve_index: int = reader.readf('H')
                    self.float_curve_index: int = reader.readf('H')
                    self.int_curve_index: int = reader.readf('H')
                    self.start_anim_const_index: int = reader.readf('H')
                    self.anim_const_count: int = reader.readf('H')
                    self.sub_bind_index: int = reader.readf('H')
                    self.name: str = reader.read_name()

            class AnimationConstant:
                def __init__(self, reader: ResReader):
                    self.anim_data_offset: int = reader.readf('I')
                    self.value: int = reader.readf('i')

    class ColorAnim(ShaderParamAnim):
        ...

    class TextureSRTAnim(ShaderParamAnim):
        ...

    class TexturePatternAnim:
        # FTXP: caFe TeXture Pattern animation
        def __init__(self, reader: ResReader):
            magic: bytes = reader.read_string(4)
            assert magic == "FTXP"

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.tell() + reader.readf('i')
            self.flags: int = reader.readf('H')
            user_data_entry_count: int = reader.readf('H')
            self.frame_count: int = reader.readf('I')
            texture_ref_count: int = reader.readf('H')
            mat_pattern_anim_count: int = reader.readf('H')
            pattern_anim_info_count: int = reader.readf('I')
            curve_count: int = reader.readf('I')
            self.baked_size: int = reader.readf('I')
            self.model_offset: int = reader.tell() + reader.readf('i')
            self.bind_index_offset: int = reader.tell() + reader.readf('i')

            self.mat_pattern_anim: List[self.MaterialPatternAnimation] = reader.read_subfiles(self.MaterialPatternAnimation, mat_pattern_anim_count)
            self.texture_refs: List[TextureReference] = reader.read_index(TextureReference)
            self.usesr_data: List[UserData] = reader.read_index(UserData)

        class MaterialPatternAnimation:
            def __init__(self, reader: ResReader):
                pattern_anim_info_count: int = reader.readf('H')
                curve_count: int = reader.readf('H')
                self.start_curve_index: int = reader.readf('i')
                self.start_pattern_anim_info_index: int = reader.readf('i')
                self.anim_name: str = reader.read_name()
                self.pattern_anim_infos: List[self.PatternAnimationInfo] = reader.read_subfiles(self.PatternAnimationInfo, pattern_anim_info_count)
                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.base_value_array_offset: int = reader.tell() + reader.readf('i')

            class PatternAnimationInfo:
                def __init__(self, reader: ResReader):
                    self.curve_index: int = reader.readf('b')
                    self.sub_bind_index: int = reader.readf('b')
                    reader.skip(2)
                    self.name: int = reader.read_name()

    class BoneVisAnim:
        # FVIS: caFe VISibility animation
        def __init__(self, reader: ResReader):
            magic: str = reader.read_string(4)
            assert magic == "FVIS"

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.tell() + reader.readf('i')
            self.flags: int = reader.readf('H')
            user_data_entry_count: int = reader.readf('H')
            self.frame_count: int = reader.readf('i')
            self.anim_count: int = reader.readf('H')
            curve_count: int = reader.readf('H')
            self.baked_size: int = reader.readf('I')
            self.model_offset: int = reader.tell() + reader.readf('i')
            self.bind_index_offset: int = reader.tell() + reader.readf('i')
            self.names_offset: int = reader.tell() + reader.readf('i')
            self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
            self.base_values_offset: int = reader.tell() + reader.readf('i')
            self.user_data_dict_offset: List[UserData] = reader.read_index(UserData)

    class MaterialVisAnim(BoneVisAnim):
        ...

    class ShapeAnim:
        # FSHA: caFe SHApe animation
        def __init__(self, reader: ResReader):
            magic: str = reader.read_string(4)
            assert magic == "FSHA"

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.tell() + reader.readf('i')
            self.flags: int = reader.readf('H')
            user_data_entry_count: int = reader.readf('H')
            self.frame_count: int = reader.readf('i')
            vertex_shape_anim_count: int = reader.readf('H')
            shape_anim_key_count: int = reader.readf('H')
            curve_count: int = reader.readf('H')
            reader.skip(2)
            self.baked_size: int = reader.readf('I')
            self.model_offset: int = reader.tell() + reader.readf('i')
            self.bind_index_offset: int = reader.tell() + reader.readf('i')
            self.vertex_shape_anims: List[self.VertexShapeAnimation] = reader.read_subfiles(self.VertexShapeAnimation, vertex_shape_anim_count)
            self.user_data: List[UserData] = reader.read_index(UserData)

        class VertexShapeAnimation:
            def __init__(self, reader: ResReader):
                curve_count: int = reader.readf('H')
                shape_anim_key_count: int = reader.readf('H')
                self.start_curve_index: int = reader.readf('i')
                self.start_shape_anim_key_index: int = reader.readf('i')
                self.name: str = reader.read_name()
                self.shape_anim_keys: List[self.ShapeAnimationKey] = reader.read_subfiles(self.ShapeAnimationKey, shape_anim_key_count)
                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.bone_transform_data_offset: int = reader.tell() + reader.readf('i')
            
            class ShapeAnimationKey:
                def __init__(self, reader: ResReader):
                    self.curve_index: int = reader.readf('b')
                    self.sub_bind_index: int = reader.readf('b')
                    reader.skip(2)
                    self.name: str = reader.read_name()

    class SceneAnim:
        # FSCN: caFe SCeNe animation
        def __init__(self, reader: ResReader):
            magic: bytes = reader.read_string(4)
            assert magic == "FSCN"

            self.file_name: str = reader.read_name()
            self.file_path_offset: int = reader.tell() + reader.readf('i')

            user_data_entry_count: int = reader.readf('H')
            camera_anim_count: int = reader.readf('H')
            light_anim_count: int = reader.readf('H')
            fog_anim_count: int = reader.readf('H')

            self.camera_anim_dict_offset: List[self.CameraAnimation] = reader.read_index(self.CameraAnimation)
            self.light_anim_dict_offset: int = reader.read_index(self.LightAnimation)
            self.fog_anim_dict_offset: int = reader.read_index(self.FogAnimation)
            self.user_data: List[UserData] = reader.read_index(UserData)
        
        class CameraAnimation:
            # FCAM: caFe CAMera animation
            def __init__(self, reader: ResReader):
                magic: str = reader.read_string(4)
                assert magic == "FCAM"
                
                self.flags: int = reader.readf('H')
                reader.skip(2)
                self.frame_count: int = reader.readf('i')
                curve_count: int = reader.readf('B')
                reader.skip(1)
                user_data_entry_count: int = reader.readf('H')
                self.baked_size: int = reader.readf('I')
                self.name: str = reader.read_name()
                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.base_cam_anim_data: self.Data = reader.read_subfile(self.Data)
                self.user_data: List[UserData] = reader.read_index(UserData)

            class Data:
                def __init__(self, reader: ResReader):
                    self.near_clipping_plane_distance: float = reader.readf('f')
                    self.far_clipping_plane_distance: float = reader.readf('f')
                    self.aspect_ratio: float = reader.readf('f')
                    self.height_offset: float = reader.readf('f')

                    self.position: Transform = Transform(reader)
                    self.rotation: Transform = Transform(reader)

                    self.twist: float = reader.readf('f')

        class LightAnimation:
            # FLIT: caFe LIghT animation
            def __init__(self, reader: ResReader):
                magic: str = reader.read_string(4)
                assert magic == "FLIT"

                self.flags: int = reader.readf('H')
                user_data_entry_count: int = reader.readf('H')
                self.frame_count: int = reader.readf('i')
                curve_count: int = reader.readf('B')
                self.light_type: int = reader.readf('b')
                self.distance_attenuation_function_index: int = reader.readf('b')
                self.angle_attenuation_function_index: int = reader.readf('b')
                self.baked_size: int = reader.readf('I')

                self.name: str = reader.read_name()
                self.light_type_name: str = reader.read_name()
                self.distance_attenuation_function_name: str = reader.read_name()
                self.angle_attenuation_function_name: str = reader.read_name()

                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.base_light_anim_data: self.Data = reader.read_subfile(self.Data)
                self.user_data: List(UserData) = reader.read_index(UserData)

            class Data:
                def __init__(self, reader: ResReader):
                    self.on_off: int = reader.readf('i')
                    self.position: Transform = Transform(reader)
                    self.rotation: Transform = Transform(reader)

                    self.distance_attenuation: Dummy = Dummy()
                    self.distance_attenuation.up: float = reader.readf('f')
                    self.distance_attenuation.down: float = reader.readf('f')

                    self.angle_attenuation: Dummy = Dummy()
                    self.angle_attenuation.right: float = reader.readf('f')
                    self.angle_attenuation.left: float = reader.readf('f')

                    self.first_color: Color = Color(reader)
                    self.second_color: Color = Color(reader)
                        
        class FogAnimation:
            # FFOG: caFe FOG animation
            def __init__(self, reader: ResReader):
                magic: str = reader.read_string(4)
                assert magic == "FFOG"

                self.flags: int = reader.readf('H')
                reader.skip(2)
                self.frame_count: int = reader.readf('i')
                curve_count: int = reader.readf('B')
                self.distance_attenuation_function_index: int = reader.readf('B')
                user_data_entry_count: int = reader.readf('H')
                self.baked_size: int = reader.readf('I')
                self.name: str = reader.read_name()
                self.distance_attenuation_function_name: str = reader.read_name()
                self.curves: List[Curve] = reader.read_subfiles(Curve, curve_count)
                self.base_fog_animation_data: self.Data = reader.read_subfile(self.Data)
                self.user_data: List[UserData] = reader.read_index(UserData)

            class Data:
                def __init__(self, reader: ResReader):
                    self.distance_attenuation = Dummy()
                    self.distance_attenuation.up: float = reader.readf('f')
                    self.distance_attenuation.down: float = reader.readf('f')

                    self.color = Color(reader)

    class EmbeddedFile:
        def __init__(self, reader: ResReader):
            self.offset: int = reader.tell() + reader.readf('i')
            self.size: int = reader.readf('I')

res_bytes = Path(sys.argv[1]).read_bytes()
if res_bytes[0:4] == b"Yaz0":
    res_bytes = oead.yaz0.decompress(res_bytes)
res_file = FRES(res_bytes)